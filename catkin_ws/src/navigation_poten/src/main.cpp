#include <iostream>
#include <boost/thread.hpp>

#include <navigation.h>
#include <CMInterface.h>

int main(int argc, char **argv)
{
    //Setting up ROS
    ros::init(argc, argv, "Navigation");
    ros::NodeHandle n;

    CMInterface interface;
    NavigationPoten *_core = new NavigationPoten(&n);

    boost::thread console_thread(&CMInterface::init, &interface, _core);
    while (!interface.terminated)
    {
        while(interface.run_map_creation)
        {
        _core->tfLoop();
        ros::spinOnce();
        }
    }
    ROS_INFO("Terminating");
    return 0;
}
