#include <CMInterface.h>

CMInterface::CMInterface()
{
    run_map_creation = false;
    terminated = false;
}
void CMInterface::init(NavigationPoten *_proj)
{
    std::string menu_text;
    menu_text = "==========================================================================================================================\n";
    menu_text += " Potential method interface active. Waiting for instructions \n";
    menu_text += " - q to quit, \n - start to run potential map creation, \n - stop to stop potential map creation,\n";
    menu_text += " - save to save map,\n - setGoal to set (x,y) goal, \n - getPath to get navigation path, \n - savePath to save navigation path. \n";
    std::cout<<menu_text;
    while (!terminated)
    {
        std::string line,cmd;
        std::getline(std::cin, line);
        std::istringstream parse (line);
        parse>>cmd;
        if (cmd == "q")
        {
            run_map_creation = false;
            terminated = true;
        }
        else if (cmd == "start")
        {
            run_map_creation = true;
        }
        else if (cmd == "stop")
        {
            std::cout<<menu_text;
            run_map_creation = false;
        }
        else if (cmd == "save")
        {
            std::cout<<"Saving map.\n";
            if(run_map_creation)
            {
                run_map_creation = false;
                std::cout<<"Map creation stopped.\n";
                _proj->saveMap();
                run_map_creation = true;
                std::cout<<"Map creation resumed.\n";
            }
            else
            {
                _proj->saveMap();
            }
        }
        else if (cmd == "setGoal")
        {
            int x, y;
            std::cout<<"x: ";
            parse>>x;
            std::cout<<"y: ";
            parse>>y;
            _proj->setGoal(x,y);
        }
        else if (cmd =="getPath")
        {
            std::cout<<"Calculating path\n";
            _proj->calculatePath();
        }
        else if (cmd =="savePath")
        {
            std::cout<<"Saving path\n";
            _proj->savePath();
        }
        else
        {
            std::cout<<"Invalid command.\n";
            std::cout<<menu_text;
        }
    }
}

CMInterface::~CMInterface()
{

}
