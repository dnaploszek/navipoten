
#include "navigation.h"

NavigationPoten::NavigationPoten(ros::NodeHandle *n)
{

    //Register subscriber
    goalX = 190;
    goalY = 169;
    startX = 116;
    startY = 180;
    sub = n->subscribe("map",1,&NavigationPoten::newMapCallback,this);

    //Register publisher
    //pub = n->advertise<sensor_msgs::PointCloud2>("processedCloud", 1);
    isMapReceived = false;
    isCalculationDone = false;
    verification = false;
}

void NavigationPoten::tfLoop()
{
    if (isMapReceived)
    {
        while (verification == false)
        {
            verification = true;
            for (int i = 0; i < map_height; i++)
            {
                for (int j = 0; j < map_width; j++)
                {
                    wavespread(i, j);
                }
            }
            ROS_INFO("Calculating...");
        }
        if(!isCalculationDone)
        {

            ROS_INFO("Creating opencv image... ");
            for (int i = 0; i < map_height; i++)
            {
                for (int j = 0; j < map_width; j++)
                {
                    map_image.at<unsigned char>(i,j) = (int)(((float)calculatedMap.at(i).at(j).value / 600) * 255);
                    //ROS_INFO("Point equals: %i", calculatedMap.at(j).at(i).value);
                    //ROS_INFO("Point open equals: %i", (int)map_image.at<unsigned char>(i,j));
                }
            }
            ROS_INFO("Created opencv image.");
            ROS_INFO("Done calculating...");
            isCalculationDone = true;
        }
    }
}

void NavigationPoten::saveMap()
{
    cv::imwrite( "map.jpg", map_image);
    ROS_INFO("Saved opencv image.");
    ROS_INFO("Map Saved");
}

void NavigationPoten::savePath()
{
    cv::Mat dst;
    cv::cvtColor(map_image, dst, CV_GRAY2RGB);
    for(int i = 0; i < path.size(); i++)
    {
        dst.at<cv::Vec3b>(path.at(i).y,path.at(i).x)[0] = 255;
        dst.at<cv::Vec3b>(path.at(i).y,path.at(i).x)[1] = 0;
        dst.at<cv::Vec3b>(path.at(i).y,path.at(i).x)[2] = 0;
    }
    cv::imwrite( "path.jpg", dst);
    ROS_INFO("Saved opencv image.");
    ROS_INFO("Path Saved");
}

void NavigationPoten::calculatePath()
{
    int i = 0;
    ROS_INFO("Start point: %i %i, Goal: %i %i", startX, startY, goalX, goalY);
    path.push_back(cv::Point2i(startX, startY));
    bool routeCompleted = false;
    while (routeCompleted ==  false)
    {
        //sprawdza czy znalazlo większego sasiada dla danego punktu, jesli tak nie szuka go dalej
        bool nextPointFound=false;
        //pętla po sasiadach bierzącego punktu trasy
        for (int n = (path.at(i).y)-1; n <= (path.at(i).y)+1; n++)
        {
            for (int m = (path.at(i).x)-1; m <= (path.at(i).x)+1; m++)
            {
                //sprawdzanie czy sasiad jest większy i jesli tak dodawanie go jako kolejnego punktu do trasy
                if (calculatedMap.at(n).at(m).value  >  calculatedMap.at(path.at(i).y).at(path.at(i).x).value && nextPointFound==false)
                {
                    i++;
                    cv::Point2i tempPoint;
                    tempPoint.x=m;
                    tempPoint.y=n;
                    path.push_back(tempPoint);
                    nextPointFound==true;
                }
            }
        }

        if (calculatedMap.at(path.at(i).y).at(path.at(i).x).value==600) //sprawdzanie czy dodany punkt nie jest celem
        {
            routeCompleted=true;
            ROS_INFO("Path created. %i steps", (int)path.size());
        }
    }
}

void NavigationPoten::wavespread(int n, int m)
{
    Field *source = &calculatedMap.at(n).at(m);

    if  (!(source->tested == false && source->value != 2 && source->value != 1)) return;
    for (int i = n-1; i <= n+1; i++)
    {
        for (int j = m-1; j <= m+1; j++)
        {
            if(calculatedMap.at(i).at(j).tested != true && i < map_height && i >= 0 && j < map_width && j >= 0)
            {
                if(calculatedMap.at(i).at(j).value < (source->value) )
                {
                    //-1 wartosc pole niewiadome, 2 wartosc pola startu, 0-przeszkoda 1- pole dostepnie nie przeliczone
                    if(calculatedMap.at(i).at(j).value != -1 && calculatedMap.at(i).at(j).value != 2 && calculatedMap.at(i).at(j).value != 0)
                    {
                        Field newField;
                        newField.value = source->value-5;
                        newField.tested = calculatedMap.at(i).at(j).tested;
                        calculatedMap.at(i).at(j) = newField;
                        verification = false;
                    }
                }
            }
        }
        source->tested=true;
    }
}

void NavigationPoten::newMapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
    if(!isMapReceived)
    {
        map = msg;
        map_width = map->info.width;
        map_height = map->info.height;
        ROS_INFO("Got map: %i x %i", map_width, map_height);

        map_image = cv::Mat::zeros( map_width, map_height, CV_8UC1 );
        for (int i = map_height; i > 0; i--)
        {
            std::vector<Field> row;
            for (int j = 0; j < map_width; j++)
            {
                int number = i*map_width + j;
                Field newField;
                newField.tested = false;
                if(map->data[number] == 0)
                {
                    newField.value = 1;
                }
                if(map->data[number] == 100)
                {
                    newField.value = 0;
                    newField.tested = true;
                }
                if(map->data[number] == -1)
                {
                    newField.value = -1;
                    newField.tested = true;
                }
                row.push_back(newField);
            }
            calculatedMap.push_back(row);
        }
        isMapReceived = true;
        verification = false;
        isCalculationDone = false;
        Field goalField;
        goalField.value = 600;
        goalField.tested = false;
        Field startField;
        startField.value = 2;
        startField.tested = true;

        calculatedMap.at(goalY).at(goalX) = goalField;
        calculatedMap.at(startY).at(startX) = startField;
    }
}

NavigationPoten::~NavigationPoten()
{

}
