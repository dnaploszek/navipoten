#ifndef CMINTERFACE_H_
#define CMINTERFACE_H_
#include <iostream>
#include <navigation.h>

class CMInterface
{	
	public:
	CMInterface();
    bool run_map_creation;
    bool terminated;
    NavigationPoten *_proj;

    void init(NavigationPoten *_proj);
	~CMInterface();
};

#endif
