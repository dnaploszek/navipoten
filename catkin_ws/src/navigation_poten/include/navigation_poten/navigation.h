#ifndef NAVIGATION_H_
#define NAVIGATION_H_

//Ros includes
#include <ros/ros.h>
//#include <tf/transform_listener.h>

//msgs includes
#include <nav_msgs/OccupancyGrid.h>
//#include "sensor_msgs/LaserScan.h"

//opencv includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

struct Field
{
public:
    int value; //wartosc pola
    bool tested;
};

class NavigationPoten
{
public:
    //Public constructor
    NavigationPoten(ros::NodeHandle *_n);

    //tf receiving and management loop
    void tfLoop();

    void setGoal(int x, int y) { goalX = x; goalY = y; }

    void saveMap();

    void calculatePath();

    void savePath();

    //Public destructor
    virtual ~NavigationPoten();

private:
    //Subscribers
    ros::Subscriber sub;

    //Publishers
    ros::Publisher pub;

    //Map
    cv::Mat map_image;
    int map_width;
    int map_height;
    std::vector<std::vector<Field> > calculatedMap;
    std::vector<cv::Point2i> path;
    bool verification;
    bool isMapReceived;
    bool isCalculationDone;
    int goalX, goalY;
    int startX, startY;
    nav_msgs::OccupancyGrid::ConstPtr map;

    //tf
    //tf::TransformListener tf_listener;
    //tf::StampedTransform odom_transform;

    void wavespread(int n, int m);

    //Subscriber callbacks:
    void newMapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);

};



#endif
